import Inferno, {linkEvent} from 'inferno';
import Component from 'inferno-component';

import Loading from './../loading/Loading';
import TicketDetail from './../TicketDetail/TicketDetail';

import EntityService from '../../entities/EntityService'

import './TicketList.css'

function getTicketByCode(obj) {
    const code = obj.code;
    const instance = obj.instance;

    instance.setState({
        loading: true,
        active: code
    });

    EntityService.getTicket(code)
        .then(
            res => {
                instance.setState({
                    detail: res,
                    loading: false,
                    error: false
                });
            },
            error => {
                instance.setState({
                    error: error,
                    loading: false
                });
            }
        );
}

class TicketList extends Component {

    constructor() {
        super();
        this.state = {
            loading: false
        };
    }

    render(props, state) {
        return (
            <div className="TicketList">
                <div className="col-sm-3">
                    <ul className="TicketList-list">
                        {
                            props.tickets.map((ticket) => (
                                <li key={ticket.code}>
                                    <a
                                        className={state.active === ticket.code ? 'active' : ''}
                                        onClick={linkEvent({code: ticket.code, instance: this}, getTicketByCode)}>
                                        {ticket.owner}
                                    </a>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <div className="col-sm-9">
                    {
                        !state.loading && !state.error ? (
                            <TicketDetail ticket={state.detail} />
                        ) : (
                            <Loading error={state.error}/>
                        )
                    }
                </div>
            </div>
        );
    }
}

export default TicketList;