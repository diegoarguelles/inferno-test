import Inferno from 'inferno';
import Component from 'inferno-component';

import TicketForm from '../form/TicketForm'

class TicketDetail extends Component {

    render(props) {

        let ticket = props.ticket;

        return (
            <div className="TicketList">
                {
                    ticket ? (
                        <div className="list-group">
                            <div className="list-group-item list-group-item-info">
                                <h3 className="list-group-item-heading text-center">{ticket.code}</h3>
                            </div>
                            <div className="list-group-item">
                                <h4 className="list-group-item-heading">Owner</h4>
                                <p className="list-group-item-text">{ticket.owner}</p>
                            </div>
                            <div className="list-group-item">
                                <h4 className="list-group-item-heading">Date</h4>
                                <p className="list-group-item-text">"{ticket.date}"</p>
                            </div>
                            <div className="list-group-item">
                                <h4 className="list-group-item-heading">Comment</h4>
                                <p className="list-group-item-text">
                                    {ticket.comment}
                                </p>
                            </div>
                            <TicketForm />
                        </div>
                    ) : (
                        <p className="lead">
                            <em>Select a ticket to see details.</em>
                        </p>
                    )
                }
            </div>
        );
    }
}

export default TicketDetail;
