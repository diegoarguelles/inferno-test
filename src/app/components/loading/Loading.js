import Inferno from 'inferno';
import Component from 'inferno-component';
import './Loading.css';

class Loading extends Component{

    render(props) {
        return(
            <div className="Loading">
                {
                    !props.error ? (
                        <img className="Loading-img" src="http://www.drivethrurpg.com/shared_images/ajax-loader.gif" alt="Loading..." />
                    ) : (
                        <p className="alert alert-danger"><strong>Error:</strong> Could not retrieve data.</p>
                    )
                }
            </div>
        );
    }
}

export default Loading;