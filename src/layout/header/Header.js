import Inferno from 'inferno';
import Component from 'inferno-component';
import './Header.css'

class Header extends Component {

    render(props) {
        return (
            <nav class="navbar navbar-default header-navbar">
                <div class="container-fluid">
                    <div class="row navbar-header">
                        {props.collapse}
                        <img className="header-logo"
                             src="https://photos.prnewswire.com/prnfull/20160527/373129LOGO"></img>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">About</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-left">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search"/>
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                        <div className="pull-right">
                            <img className="user-picture"
                                 src="http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons-256/glossy-black-icons-animals/012600-glossy-black-icon-animals-animal-dog4.png"></img>
                            <label>Welcome, Diego</label>
                        </div>

                    </div>
                </div>
            </nav>
        );
    }
}

export default Header;